﻿namespace App2.Droid
{
    using Android.App;
    using Android.Widget;
    using Android.OS;
    using Android.Support.Design.Widget;
    using App2.Droid.Helpers;
    using System;
    using Android.Views;
    using Android.Support.V7.View.Menu;
    using App2.Droid.Adapters;
    using Android.Support.V4.View;
    using Android.Support.V4.App;
    using App2.Droid.Fragments;

    [Activity(Label = "App2", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : FragmentActivity
    {
        RelativeLayout rootView;
        private BottomNavigationView mBottomNavigationView;
        private static TextView txtLabel;
        private SectionsStatePagerAdapter mSectionSpacePagerAdapter;
        private ViewPager mViewPager;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.ColoresXample);


            // Fragment
            mSectionSpacePagerAdapter = new SectionsStatePagerAdapter(SupportFragmentManager);
            mViewPager = FindViewById<ViewPager>(Resource.Id.viewPagerFragment);


            rootView = FindViewById<RelativeLayout>(Resource.Id.layoutMain);

            //txtLabel = FindViewById<TextView>(Resource.Id.textLabel);

            ImageView imageMenu = FindViewById<ImageView>(Resource.Id.menu);

            imageMenu.Click += (sender, e) =>
            {
                //txtLabel.Text = "Click in Menu";
                ShowPopup(imageMenu);
            };

            mBottomNavigationView = FindViewById<BottomNavigationView>(Resource.Id.bottomNavigationView);
            BottomNavigationViewHelper.DisableShiftMode(mBottomNavigationView);
            mBottomNavigationView.NavigationItemSelected += mBottomNavigationView_NavigationItemSelected;

            FindViewById<ImageView>(Resource.Id.buscar).Click += (sender, e) =>
            {
                //txtLabel.Text = "Click in search";
            };


            FindViewById<ImageView>(Resource.Id.menu).Click += (sender, e) =>
            {
                //txtLabel.Text = "Click in menu";
            };

            SetupViewPager(mViewPager);

        }



        private void ShowPopup(View v)
        {
            MenuBuilder menuBuilder = new MenuBuilder(this);
            MenuInflater inflater = new MenuInflater(this);
            inflater.Inflate(Resource.Menu.bottom_menu, menuBuilder);
            MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, v);
            optionsMenu.SetForceShowIcon(true);
            optionsMenu.Show();

            //Listener

            menuBuilder.SetCallback(new IMenuBuilder());
            
        }

        // Interface que atiende los eventos del menu popup
        class IMenuBuilder : Java.Lang.Object, MenuBuilder.ICallback
        {
            public Action HandleSucces { get; set; }

            public bool OnMenuItemSelected(MenuBuilder mBuilder, IMenuItem item)
            {
                switch (item.ItemId)
                {
                    case Resource.Id.maps:
                        // Implement 
                        //txtLabel.Text = "Click en Mapas";
                        break;
                    case Resource.Id.linterna:
                        // Implement
                        //txtLabel.Text = "Click en Linterna";
                        break;
                    case Resource.Id.adelante:
                        // Implement
                        //txtLabel.Text = "Click en Nuevo";
                        break;
                    case Resource.Id.texto:
                        // Implement 
                        //txtLabel.Text = "Click en Modal";
                        break;
                    default:
                        break;
                }
                return true;
            }

            public void OnMenuModeChange(MenuBuilder mBuilder) { }
        }


        // Interface que atiende los eventos del menu nav
        private void mBottomNavigationView_NavigationItemSelected(object sender, BottomNavigationView.NavigationItemSelectedEventArgs e)
        {
            

            switch (e.Item.ItemId)
            {
                case Resource.Id.maps:
                    e.Item.SetChecked(true);
                    SetViewPager(0);
                    break;
                case Resource.Id.linterna:
                    e.Item.SetChecked(true);
                    SetViewPager(1);
                    //txtLabel.Text = "Click en linterna";
                    break;
                case Resource.Id.adelante:
                    e.Item.SetChecked(true);
                    // Ejemplo de actividad
                    StartActivity(new Android.Content.Intent(this, typeof(NewActivity)));
                    //txtLabel.Text = "Click en adelante";
                    break;
                case Resource.Id.texto:
                    e.Item.SetChecked(true);
                    //txtLabel.Text = "Click en libritos";
                    break;
            }
        }

        private void SetupViewPager(ViewPager viewPager)
        {
            SectionsStatePagerAdapter adapter = new SectionsStatePagerAdapter(SupportFragmentManager);
            adapter.addFragment(new FragmentMaps(), "Mapas");
            adapter.addFragment(new FragmentLinterna(), "Linterna");


            mViewPager.Adapter = adapter;
        }

        private void SetViewPager(int fragmentNumber)
        {
            mViewPager.CurrentItem = fragmentNumber;
        }
    }
}

