﻿namespace App2.Droid.Adapters
{
 
    using System;
    using System.Collections.Generic;
    using Android.Support.V4.App;

    public class SectionsStatePagerAdapter : FragmentStatePagerAdapter
    {
        private List<Fragment> mFragmentList = new List<Fragment>();
        private List<String> mFragmentTitleList = new List<String>();


        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.Add(fragment);
            mFragmentTitleList.Add(title);
        }

        public SectionsStatePagerAdapter(FragmentManager fm)
            : base(fm) { }


        public override int Count
        {
            get { return mFragmentList.Count; }
        }

        public override Fragment GetItem(int position)
        {
            return mFragmentList[position];
        }
    }
}