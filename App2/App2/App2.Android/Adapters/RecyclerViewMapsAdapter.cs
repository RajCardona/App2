﻿using System.Collections.Generic;
using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App2.Models;
namespace App2.Droid.Adapters
{
    public class RecyclerViewMapsAdapter : RecyclerView.Adapter
    {
        private List<PointMaps> points;
        Context context;

        public RecyclerViewMapsAdapter(List<PointMaps> points)
        {
            this.points = points;
            NotifyDataSetChanged();
        }

        public class RecyclerViewHolder : RecyclerView.ViewHolder
        {

            public TextView contentName;
            public TextView contentLon;
            public TextView contentLat;

            public RecyclerViewHolder(View itemView) : base(itemView)
            {
                contentName = itemView.FindViewById<TextView>(Resource.Id.txtName);
                contentLon = itemView.FindViewById<TextView>(Resource.Id.txtLon);
                contentLat = itemView.FindViewById<TextView>(Resource.Id.txtLat);
            }
        }
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View listitem = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.points_layout, parent, false);
            RecyclerViewHolder v = new RecyclerViewHolder(listitem);
            return v;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            RecyclerViewHolder recyclerViewHolder = holder as RecyclerViewHolder;
            recyclerViewHolder.contentName.Text = points[position].Name;
            recyclerViewHolder.contentLon.Text = points[position].Longitude.ToString();
            recyclerViewHolder.contentLat.Text = points[position].Longitude.ToString();
        }
        public override int ItemCount
        {
            get { return points.Count; }
        }

    }


}
