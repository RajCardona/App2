﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace App2.Droid
{
    [Activity(Label = "NewActivity", MainLauncher = false,
        Theme="@style/Theme.AppCompat.Light.NoActionBar")]
    public class NewActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_layout);
            // Create your application here

            var ToolbarMenu = FindViewById<Toolbar>(Resource.Id.toolbarNew);
            SetSupportActionBar(ToolbarMenu);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.Title = "Nueva Actividad";
            SupportActionBar.SetIcon(Resource.Drawable.chiquito);
            //SupportActionBar.SetDisplayShowHomeEnabled(true);
        }

        public override bool OnSupportNavigateUp()
        {
            //OnBackPressed();
            Finish();
            return true;
        }
    }
}