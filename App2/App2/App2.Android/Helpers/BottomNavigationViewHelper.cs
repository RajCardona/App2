﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Support.Design.Internal;
using Android.Support.Design.Widget;
using Android.Views;

namespace App2.Droid.Helpers
{
    public class BottomNavigationViewHelper
    {

        private static ColorStateList navMenuTextListUnChecked = new ColorStateList(
            new int[][]{
            new[] { global::Android.Resource.Attribute.StateEnabled },
            new[] { -global::Android.Resource.Attribute.StateEnabled },
            },
            new int[]{
                Color.ParseColor("#E0E0E0"),
                Color.ParseColor("#E0E0E0"),}
        );

        private static ColorStateList navMenuTextListChecked = new ColorStateList(
            new int[][]{
            new[] { global::Android.Resource.Attribute.StateEnabled },
            new[] { -global::Android.Resource.Attribute.StateEnabled },
            },
            new int[]{
                Color.ParseColor("#58B660"),
                Color.ParseColor("#58B660"),}
            );

        public static void DisableShiftMode(BottomNavigationView view)
        {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView)view.GetChildAt(0);
            try
            {
                Java.Lang.Reflect.Field shiftingMode = menuView.Class.GetDeclaredField("mShiftingMode");
                shiftingMode.Accessible = true;
                shiftingMode.Set(menuView, false);
                shiftingMode.Accessible = false;
                for (int i = 0; i < menuView.ChildCount; i++)
                {
                    BottomNavigationItemView item = (BottomNavigationItemView)menuView.GetChildAt(i);
                    item.SetShiftingMode(false);
                    item.SetChecked(item.ItemData.IsChecked);
                }
                menuView.UpdateMenuView();
            }
            catch (Exception ex) { }
        }

        public static void EnableSelectedItem(BottomNavigationView view)
        {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView)view.GetChildAt(0);
            try
            {
                Java.Lang.Reflect.Field shiftingMode = menuView.Class.GetDeclaredField("mShiftingMode");
                shiftingMode.Accessible = true;
                shiftingMode.SetBoolean(menuView, false);
                shiftingMode.Accessible = false;

                IMenu menu = view.Menu;
                for (int i = 0; i < menuView.ChildCount; i++)
                {
                    BottomNavigationItemView item = (BottomNavigationItemView)menuView.GetChildAt(i);
                    IMenuItem itemM = menu.GetItem(i);
                    if (itemM.IsChecked)
                    {
                        item.SetTextColor(navMenuTextListChecked);
                        item.SetIconTintList(navMenuTextListChecked);
                    }
                    else
                    {
                        item.SetTextColor(navMenuTextListUnChecked);
                        item.SetIconTintList(navMenuTextListUnChecked);
                    }

                }
            }
            catch (Exception ex) { }
        }
    }
}
