﻿namespace App2.Droid.Fragments
{
    using Android.OS;
    using Android.Views;
    using Android.Support.V4.App;

    public class FragmentLinterna :Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_linterna, container, false);
            return view;
        }

    }
}