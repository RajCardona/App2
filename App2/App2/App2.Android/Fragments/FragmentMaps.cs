﻿using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using App2.Droid.Adapters;
using App2.Models;
using System.Collections.Generic;

namespace App2.Droid.Fragments
{
    public class FragmentMaps : Fragment
    {
        private RecyclerView recyclerViewContents;
        private LinearLayoutManager linearLayoutManager;
        private List<PointMaps> mList;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_maps, container, false);

            // Definimos La Lista
            recyclerViewContents = view.FindViewById<RecyclerView>(Resource.Id.recyclerMaps);
            linearLayoutManager = new LinearLayoutManager(this.Activity);
            recyclerViewContents.SetLayoutManager(linearLayoutManager);
            recyclerViewContents.HasFixedSize = true;
            InitializeData();
            initializeAdapter();

            return view;
        }

        public void InitializeData()
        {
            mList = new List<PointMaps>();
            mList = PointMaps.GetListPoints();
        }

        public void initializeAdapter()
        {
            // Inicializo el Adaptador que inflara la lista
            RecyclerViewMapsAdapter adapter = new RecyclerViewMapsAdapter(mList);
            recyclerViewContents.SetAdapter(adapter);
        }

    }
}