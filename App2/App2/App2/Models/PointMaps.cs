﻿using System;
using System.Collections.Generic;

namespace App2.Models
{
    public class PointMaps
    {
        public String Name { get; set; }
        public Double Longitude { get; set; }
        public Double Latitude { get; set; }


        public static List<PointMaps> GetListPoints()
        {
            List<PointMaps> mList = new List<PointMaps>();
            PointMaps point = new PointMaps();
            point.Name = "El Dorado - Envigado";
            point.Longitude = 6.1633557;
            point.Latitude = -75.5939171;
            mList.Add(point);

            point = new PointMaps();
            point.Name = "La Paz - Envigado";
            point.Longitude = 6.1616489;
            point.Latitude = -75.5956887;
            mList.Add(point);

            point = new PointMaps();
            point.Name = "Trianon - Envigado";
            point.Longitude = 6.1562754;
            point.Latitude = -75.5971317;
            mList.Add(point);

            return mList;
        }

    }
}

